CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Git Issues module provides a Git core API layer for managing git issue
from your Drupal website.

Features:
 * List opened and closed issues on your Drupal site.
 * Create, edit, and close issues from your Drupal site.

 * For a full description of the module visit:
   https://www.drupal.org/project/git_issues

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/git_issues


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

The following are plugins for the Git Issues module:
* Bitbucket Issues - https://www.drupal.org/project/bitbucket_issues
* GitHub Issues - https://www.drupal.org/project/github_issues


INSTALLATION
------------

 * Install the Git Issues module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Issues to populate module settings form
       with the git base url and git user access token.
    3. Save.


MAINTAINERS
-----------

 * Petar Gnjidic (petar.gnjidic) - https://www.drupal.org/u/petargnjidic
 * Bojan Živkov - https://www.drupal.org/u/bojan-%C5%BEivkov

Supporting organization:
 * Circle Web Foundry - https://www.drupal.org/circle-web-foundry
